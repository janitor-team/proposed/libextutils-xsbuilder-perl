Source: libextutils-xsbuilder-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Ansgar Burchardt <ansgar@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: perl
Standards-Version: 3.8.4
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libextutils-xsbuilder-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libextutils-xsbuilder-perl.git
Homepage: https://metacpan.org/release/ExtUtils-XSBuilder

Package: libextutils-xsbuilder-perl
Architecture: all
Depends: ${misc:Depends},
         libparse-recdescent-perl,
         libtie-ixhash-perl,
         ${perl:Depends}
Multi-Arch: foreign
Description: Automatic XS glue code generation
 ExtUtils::XSBuilder is a set of modules to parse C header files and
 create XS glue code and documentation from it.
 .
 Ideally this allows you to "write" an interface to a C library
 without coding a line.  Although this module automates much of the
 process, you must still be familiar with C and XS programming since no
 C API is ideal and usually some adjuments are necessary.  When the C
 API changes, most of the time you only have to rerun XSBuilder to get
 your new Perl API.
